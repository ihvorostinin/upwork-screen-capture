//
//  ViewController.swift
//  inScreen
//
//  Created by Ivan Khvorostinin on 03/05/2017.
//  Copyright © 2017 Golden Flag America, LLC. All rights reserved.
//

import Cocoa
import AVFoundation
import CoreGraphics

class ISCaptureController: NSObject, AVCaptureFileOutputRecordingDelegate
{
    
    var session: AVCaptureSession?
    var output: AVCaptureMovieFileOutput?

    func start() -> Bool
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .medium

        let pathDocuments = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory,
                                                                FileManager.SearchPathDomainMask.userDomainMask,
                                                                true)[0]
        let path = (pathDocuments as NSString).appendingPathComponent(dateFormatter.string(from: Date()) + ".mp4")

        // Create a capture session
        session = AVCaptureSession()
        
        // Set the session preset as you wish
        session!.sessionPreset = AVCaptureSessionPresetHigh
        
        // If you're on a multi-display system and you want to capture a secondary display,
        // you can call CGGetActiveDisplayList() to get the list of all active displays.
        // For this example, we just specify the main display.
        // To capture both a main and secondary display at the same time, use two active
        // capture sessions, one for each display. On Mac OS X, AVCaptureMovieFileOutput
        // only supports writing to a single video track.
        let displayId = CGMainDisplayID()
        
        // Create a ScreenInput with the display and add it to the session
        let input = AVCaptureScreenInput(displayID: displayId)
        
        if (input == nil)
        {
            session = nil
            return false
        }
        
        if (session!.canAddInput(input))
        {
            session!.addInput(input)
        }
        else
        {
            session = nil
            return false
        }
        
        // Create a MovieFileOutput and add it to the session
        output = AVCaptureMovieFileOutput()
        
        if (session!.canAddOutput(output))
        {
            session!.addOutput(output)
        }
        else
        {
            session = nil
            output = nil
            return false
        }
        
        // Start running the session
        session!.startRunning()
        
        // Start recording to the destination movie file
        // The destination path is assumed to end with ".mov", for example, @"/users/master/desktop/capture.mov"
        // Set the recording delegate to self
        output!.startRecording(toOutputFileURL: URL(fileURLWithPath: path), recordingDelegate: self)
        
        return true
    }
    
    func stopp()
    {
        output?.stopRecording()
        output = nil
        session = nil
    }
    
    public func capture(_ captureOutput: AVCaptureFileOutput!,
                        didFinishRecordingToOutputFileAt outputFileURL: URL!,
                        fromConnections connections: [Any]!,
                        error: Error!)
    {
        session?.stopRunning();
        output = nil
        session = nil
    }

}

