//
//  Menu.swift
//  inScreen
//
//  Created by Ivan Khvorostinin on 03/05/2017.
//  Copyright © 2017 Golden Flag America, LLC. All rights reserved.
//

import Foundation
import Cocoa

extension NSMenu
{
    func replaceItem(_ item: NSMenuItem, with newItem: NSMenuItem)
    {
        let i = index(of: item)
        
        removeItem(item)
        insertItem(newItem, at: i)
    }
}
