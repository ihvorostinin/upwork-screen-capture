//
//  AppDelegate.swift
//  inScreen
//
//  Created by Ivan Khvorostinin on 03/05/2017.
//  Copyright © 2017 Golden Flag America, LLC. All rights reserved.
//

import Cocoa


@NSApplicationMain
class ISAppDelegate: NSObject, NSApplicationDelegate
{

    static let StatusItemWidth = CGFloat(24.0)
    static let StatusItemImageNormal = "status_bar_icon_normal"
    static let StatusItemImageAltern = "status_bar_icon_alternative"
    
    static let MenuCaptureStart = "Capture"
    static let MenuCaptureStopp = "Stop recording"
    
    let captureController = ISCaptureController()
    
    var statusItem: NSStatusItem?
    var menuRecordingStart: NSMenuItem?
    var menuRecordingStopp: NSMenuItem?

    func applicationDidFinishLaunching(_ aNotification: Notification)
    {
        statusItem = NSStatusBar.system().statusItem(withLength: ISAppDelegate.StatusItemWidth)

        statusItem!.button!.image = NSImage(named: ISAppDelegate.StatusItemImageNormal)
        statusItem!.button!.alternateImage = NSImage(named: ISAppDelegate.StatusItemImageAltern)
        
        menuRecordingStart = NSMenuItem(title: ISAppDelegate.MenuCaptureStart,
                                         action: #selector(_startCapture),
                                         keyEquivalent: "")

        menuRecordingStopp = NSMenuItem(title: ISAppDelegate.MenuCaptureStopp,
                                        action: #selector(_stoppCapture),
                                        keyEquivalent: "")

        statusItem!.menu = NSMenu()
        statusItem!.menu!.addItem(menuRecordingStart!)
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }

    func _startCapture()
    {
        if (captureController.start())
        {
            statusItem!.menu!.replaceItem(menuRecordingStart!, with: menuRecordingStopp!)
        }
    }

    func _stoppCapture()
    {
        statusItem!.menu!.replaceItem(menuRecordingStopp!, with: menuRecordingStart!)
        captureController.stopp()
    }
}

